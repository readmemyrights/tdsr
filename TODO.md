# TODO

As things get done remove them from this list.

# DO IT ASAP

- Figure out the new regarded broken python way of handling dependencies and try
  to make this program conform to it. I had to make a fucking virtual
  environment because the people maintaining python are more interested in
  solving theoretical problems than maintaining compatibility and not being an
  asshole to their users.

# Niceties

* Handle basic attributes, at least having something like m-f to say whether the
  current character is highlighted in some way. Fenrir and speakup have this
  already.
* An extension of the above, have something like highlight reporting mode, to fix
  broken inaccessible programs that won't to reinvent the terminal cursor. There
  are whole TUI libraries that make this sort of mistake.
- Write manual pages: tdsr(1), tdsr_protocol(7)? Should tdsr.cfg(5) be its own
  manual page or a part of tdsr(1)?
  There's beginning work on tdsr(1) but it's still a long way off, I really need
  to recruit more people to work on this while I'm busy with other stuff.
- usage examples: how to incorporate into various shell initialization files,
  perhaps a whole "blind terminal users" wiki is in order? That would be out
  of the scope of this project though.
- More example plugins, it would be interesting to have a repository of them, if
    someone hasn't done that yet.
- Add support for unixy plugins, executables that read from stdin and write to
    stdout.
- Get tdsr into various package repositories. This will probably require setting
  up a needless makefile, start properly tagging releases and other such
  bureaucracy.
- Think about the current way of handling keys, and also reconsider the default
  keybindings. Every once in a while I find a program they conflict with and
  it's annoying to say the least. We probably need a prefix key.
- get it included in installation isos of various
  distributions. I'm not going to pretend like speakup is any good, in fact
  whoever thought that having a screen reader + espeak inside of kernelspace is a good
  idea needs to get checked.

# Long term decisions

- Rewrite in X? I don't want any weird pythonic surprises down the line.
  Of course this will probably have to wait until the featureset is stable, and
  I don't want to break off from the upstream just yet, at least until it
  becomes clear that tspivey doesn't care.

Note: the above thing was written when I was high on my own farts. Tspivey cares
much more about this piece of shit program than I ever will and just by checking
pull requests and merging them, unlike me. An apology is in order.
