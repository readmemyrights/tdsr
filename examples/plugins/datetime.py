import time

lastcalltime = 0 # Hopefully early enough

def parse_output(lines):
	global lastcalltime

	currentcalltime = time.monotonic()

	if currentcalltime - lastcalltime < 0.3:
		r = time.strftime('%Y-%m-%d')
	else:
		r = time.strftime('%H:%M:%S')

	lastcalltime = currentcalltime
	return [r]
